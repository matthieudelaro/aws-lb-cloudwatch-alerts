/* ------------------------------------------------------------------------------------------------------------ */
/* Context variables                                                                                             */
/* ------------------------------------------------------------------------------------------------------------ */
variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name"
  default     = ""
}

variable "stage" {
  type        = string
  description = "Stage, which can be 'prod', 'staging', 'dev'..."
  default     = ""
}
/* ------------------------------------------------------------------------------------------------------------ */


variable "load_balancer_arn_suffix" {
  type        = string
  description = "Load balancer ARN suffix"
}

variable "target_group_service_arn_suffix" {
  type        = list(string)
  description = "A list of Target group ARN suffix. They are the the ECS services to monitor healthy check"
}


/* ------------------------------------------------------------------------------------------------------------ */
/*  When healtcheck on service tasks is not ok */
/* ------------------------------------------------------------------------------------------------------------ */
variable "unheathy_service_task_alarm_name" {
  type        = string
  description = "Description of the Alarm to alert when service task healthcheck is not ok"
  default     = "Unheathy-Target-Service"
}

variable "unhealthy_service_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold. Applied to the alert when service is unhealthy"
  default     = 3
}

variable "unhealthy_service_period" {
  type        = number
  description = "Period in second to query metrics used for alert when service is unhealthy"
  default     = 60
}

variable "unhealthy_service_threshold" {
  type        = string
  description = "Http error rate that trigger http_error alarm"
  default     = 0
}

variable "unhealthy_service_alarm_actions" {
  type        = list(string)
  description = "The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed."
  default     = []
}
/* ------------------------------------------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------------------------------------------ */
/*  When external http request does not send a 200 on multiple routes */
/* ------------------------------------------------------------------------------------------------------------ */
variable "error_http_routes_task_alarm_name" {
  type        = string
  description = "Description of the Alarm to alert when external http request does not send a 2xx on multiple routes"
  default     = "Error_HTTP_Routes"
}


variable "error_http_routes_evaluation_periods" {
  type        = number
  description = "The number of periods over which data is compared to the specified threshold. Applied to the alert when external http request does not send a 2xx on multiple routes"
  default     = 3
}

variable "error_http_routes_period" {
  type        = string
  description = "Period in second to query metrics used for alerts"
  default     = 60
}

variable "error_http_alarm_threshold" {
  type        = string
  description = "Http error rate (percent) that trigger http_error alarm"
  default     = 60
}

variable "error_http_routes_alarm_actions" {
  type        = list(string)
  description = "The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed."
  default     = []
}
/* ------------------------------------------------------------------------------------------------------------ */
