# Aws load balancer cloudwatch alerts

This module :

- Create alarm to alert when healtcheck on service tasks is not ok
- Alert when external http request does not send a 200 on multiple routes

## Usage

```hcl-terraform
data "aws_lb" "my_lb" {
  name                              = "my_lb"
}
data "aws_lb_target_group" "front_tg" {
  name                              = "front-tg"
}
data "aws_lb_target_group" "back_tg" {
  name                              = "back-tg"
}

module "lb_cloudwath_alerts" {
  source                            = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-lb-cloudwatch-alerts"
  namespace                         = "devops"
  stage                             = "prod"
  load_balancer_arn_suffix          = data.aws_lb.my_lb.arn_suffix
  target_group_service_arn_suffix = [
    data.aws_lb_target_group.front_tg.arn_suffix,
    data.aws_lb_target_group.back_tg.arn_suffix
  ]
}
```

To subscribe to Alarm state, we need to provide a list of ARN SNS topic.

```hcl-terraform
module "sns_subscription_error_http_routes" {
  source                            = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-sns-email-subscription"
  sns_topic_name                    = "SNS_TOPIC_ERROR_HTTP_ROUTES"
  user_emails                       = ["user@example.com"]
}
module "sns_subscription_unhealthy_service" {
  source                            = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-sns-email-subscription"
  sns_topic_name                    = "SNS_TOPIC_UNHEALTHY_SERVICES"
  user_emails                       = ["user@example.com"]
}

data "aws_lb" "my_lb" {
  name                              = "my_lb"
}
data "aws_lb_target_group" "front_tg" {
  name                              = "front-tg"
}
data "aws_lb_target_group" "back_tg" {
  name                              = "back-tg"
}

module "lb_cloudwath_alerts" {
  source                            = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-lb-cloudwatch-alerts"
  namespace                         = "devops"
  stage                             = "prod"
  load_balancer_arn_suffix          = data.aws_lb.my_lb.arn_suffix
  
  error_http_routes_alarm_actions   = [module.sns_subscription_error_http_routes.aws_sns_topic_arn]
  unhealthy_service_alarm_actions   = [module.sns_subscription_unhealthy_service.aws_sns_topic_arn]

  target_group_service_arn_suffix = [
    data.aws_lb_target_group.front_tg.arn_suffix,
    data.aws_lb_target_group.back_tg.arn_suffix
  ]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_metric_alarm.error_http_routes](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.unhealthy_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_error_http_alarm_threshold"></a> [error\_http\_alarm\_threshold](#input\_error\_http\_alarm\_threshold) | Http error rate (percent) that trigger http\_error alarm | `string` | `60` | no |
| <a name="input_error_http_routes_alarm_actions"></a> [error\_http\_routes\_alarm\_actions](#input\_error\_http\_routes\_alarm\_actions) | The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed. | `list(string)` | `[]` | no |
| <a name="input_error_http_routes_evaluation_periods"></a> [error\_http\_routes\_evaluation\_periods](#input\_error\_http\_routes\_evaluation\_periods) | The number of periods over which data is compared to the specified threshold. Applied to the alert when external http request does not send a 2xx on multiple routes | `number` | `3` | no |
| <a name="input_error_http_routes_period"></a> [error\_http\_routes\_period](#input\_error\_http\_routes\_period) | Period in second to query metrics used for alerts | `string` | `60` | no |
| <a name="input_error_http_routes_task_alarm_name"></a> [error\_http\_routes\_task\_alarm\_name](#input\_error\_http\_routes\_task\_alarm\_name) | Description of the Alarm to alert when external http request does not send a 2xx on multiple routes | `string` | `"Error_HTTP_Routes"` | no |
| <a name="input_load_balancer_arn_suffix"></a> [load\_balancer\_arn\_suffix](#input\_load\_balancer\_arn\_suffix) | Load balancer ARN suffix | `string` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name | `string` | `""` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, which can be 'prod', 'staging', 'dev'... | `string` | `""` | no |
| <a name="input_target_group_service_arn_suffix"></a> [target\_group\_service\_arn\_suffix](#input\_target\_group\_service\_arn\_suffix) | A list of Target group ARN suffix. They are the the ECS services to monitor healthy check | `list(string)` | n/a | yes |
| <a name="input_unhealthy_service_alarm_actions"></a> [unhealthy\_service\_alarm\_actions](#input\_unhealthy\_service\_alarm\_actions) | The list of actions, e.g. ARNs of SNS topic, to execute when this alarm transisions into ALARM state. Up to 5 actions are allowed. | `list(string)` | `[]` | no |
| <a name="input_unhealthy_service_evaluation_periods"></a> [unhealthy\_service\_evaluation\_periods](#input\_unhealthy\_service\_evaluation\_periods) | The number of periods over which data is compared to the specified threshold. Applied to the alert when service is unhealthy | `number` | `3` | no |
| <a name="input_unhealthy_service_period"></a> [unhealthy\_service\_period](#input\_unhealthy\_service\_period) | Period in second to query metrics used for alert when service is unhealthy | `number` | `60` | no |
| <a name="input_unhealthy_service_threshold"></a> [unhealthy\_service\_threshold](#input\_unhealthy\_service\_threshold) | Http error rate that trigger http\_error alarm | `string` | `0` | no |
| <a name="input_unheathy_service_task_alarm_name"></a> [unheathy\_service\_task\_alarm\_name](#input\_unheathy\_service\_task\_alarm\_name) | Description of the Alarm to alert when service task healthcheck is not ok | `string` | `"Unheathy-Target-Service"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alarm_error_http_routes_arn"></a> [alarm\_error\_http\_routes\_arn](#output\_alarm\_error\_http\_routes\_arn) | A list of ARN of the cloudwatch metric alarms to alert when external http request does not send a 2xx on multiple routes |
| <a name="output_alarm_unhealthy_service_arn"></a> [alarm\_unhealthy\_service\_arn](#output\_alarm\_unhealthy\_service\_arn) | A list of ARN of the cloudwatch metric alarms to alert when some service are not healthy on load balancer target groups |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
