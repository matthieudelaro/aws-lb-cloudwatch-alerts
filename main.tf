locals {
  # This is the service name registered as target group for the Load balancer
  # It is required with the following Cloudwatch alarms:
  # - unhealthy_service: When healtcheck on service tasks is not ok
  # - error_http_routes: When external http request does not send a 200 on multiple routes
  target_group_service_names = [for s in var.target_group_service_arn_suffix : split("/", s)[1]]
}

/* ----------------------------------------------------------------------------------------- */
/*  Usage: Alert when healtcheck on service tasks is not ok                                  */
# Metric Detail:
# This alarm monitors the metric HealthyHostCount under AWS/ApplicationELB namespace
# This metric can only have either value 1 (service task is heathy) or 0 (service task is unheathy)
# To use this correctly, we should ensure to setup heath check for ECS task
# Further docs: https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-cloudwatch-metrics.html
/* ----------------------------------------------------------------------------------------- */
locals {
  unheathy_service_task_alarm_names = [for s in local.target_group_service_names : "${var.namespace}/${var.stage}/${var.unheathy_service_task_alarm_name}/${s}"]
}

# Note: The metric HealthyHostCount returns 0 if the service is unhealthy, and return 1 if the service is heaththy
# The heathcheck configuration is related to target group registered with the Load Balancer
resource "aws_cloudwatch_metric_alarm" "unhealthy_service" {
  count = length(local.unheathy_service_task_alarm_names)

  alarm_name                = local.unheathy_service_task_alarm_names[count.index]
  comparison_operator       = "LessThanOrEqualToThreshold"
  evaluation_periods        = var.unhealthy_service_evaluation_periods
  metric_name               = "HealthyHostCount"
  namespace                 = "AWS/ApplicationELB"
  period                    = var.unhealthy_service_period
  statistic                 = "Minimum"
  threshold                 = var.unhealthy_service_threshold
  alarm_description         = "This metric monitors the unhealthy target of the service ${local.target_group_service_names[count.index]}"
  insufficient_data_actions = []
  alarm_actions             = var.unhealthy_service_alarm_actions

  # Requires to use ARN suffix
  dimensions = {
    TargetGroup  = var.target_group_service_arn_suffix[count.index]
    LoadBalancer = var.load_balancer_arn_suffix
  }

}
/* ----------------------------------------------------------------------------------------- */


/* ----------------------------------------------------------------------------------------- */
/*  Usage: Alert when external http request does not send a 200 on multiple routes           */
# Metric Detail:
# This alarm is set based on the calculation of these metric queries:
## - m1: Number of HTTP 4XX count in 1 minute
## - m2: Number of HTTP 5XX count in 1 minute
## - m4: Number of HTTP 2XX count in 1 minute
## - m5: Number of HTTP 3XX count in 1 minute

# With the above queried metrics, we calculated the Error HTTP rate as follow:
# (m1+m2)*100/(m1+m2+m3+m4)
# If the rate is over threshold (60%), the alarm is triggered
/* ----------------------------------------------------------------------------------------- */
locals {
  error_http_routes_task_alarm_names = [for s in local.target_group_service_names : "${var.namespace}/${var.stage}/${var.error_http_routes_task_alarm_name}/${s}"]
}

# Here we only measure HTTP code per target (which is equilivalent to a serice inside ECS cluster)
resource "aws_cloudwatch_metric_alarm" "error_http_routes" {
  count = length(local.error_http_routes_task_alarm_names)

  alarm_name          = local.error_http_routes_task_alarm_names[count.index]
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.error_http_routes_evaluation_periods

  threshold                 = var.error_http_alarm_threshold
  alarm_description         = "This alarm alert if there are numerous error HTTP routes of the service ${local.target_group_service_names[count.index]}"
  insufficient_data_actions = []
  alarm_actions             = var.error_http_routes_alarm_actions

  metric_query {
    id          = "e1"
    expression  = "(m1+m2)*100/(m1+m2+m3+m4)"
    label       = "Error HTTP Rate"
    return_data = "true"
  }

  metric_query {
    id = "m1"

    metric {
      metric_name = "HTTPCode_Target_4XX_Count"
      namespace   = "AWS/ApplicationELB"
      period      = var.error_http_routes_period
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        TargetGroup  = var.target_group_service_arn_suffix[count.index]
        LoadBalancer = var.load_balancer_arn_suffix
      }
    }
  }

  metric_query {
    id = "m2"

    metric {
      metric_name = "HTTPCode_Target_5XX_Count"
      namespace   = "AWS/ApplicationELB"
      period      = var.error_http_routes_period
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        TargetGroup  = var.target_group_service_arn_suffix[count.index]
        LoadBalancer = var.load_balancer_arn_suffix
      }
    }
  }

  metric_query {
    id = "m3"

    metric {
      metric_name = "HTTPCode_Target_2XX_Count"
      namespace   = "AWS/ApplicationELB"
      period      = var.error_http_routes_period
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        TargetGroup  = var.target_group_service_arn_suffix[count.index]
        LoadBalancer = var.load_balancer_arn_suffix
      }
    }
  }

  metric_query {
    id = "m4"

    metric {
      metric_name = "HTTPCode_Target_3XX_Count"
      namespace   = "AWS/ApplicationELB"
      period      = var.error_http_routes_period
      stat        = "Sum"
      unit        = "Count"

      dimensions = {
        TargetGroup  = var.target_group_service_arn_suffix[count.index]
        LoadBalancer = var.load_balancer_arn_suffix
      }
    }
  }
}
/* ----------------------------------------------------------------------------------------- */
